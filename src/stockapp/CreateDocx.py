from docx import Document
from bokeh.plotting import figure
from bokeh.io import export_png
from docx.shared import Inches
from docx.oxml.ns import nsdecls
from docx.oxml import parse_xml
from docx.enum.table import WD_TABLE_ALIGNMENT
import os

from stockreportgen.settings import STATICFILES_DIRS


def creategrowthtable(table, res):
    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    table.style.font.cs_bold = True
    cells = table.rows[0].cells
    shading_elm_1 = parse_xml(r'<w:shd {} w:fill="a9a9a9"/>'.format(nsdecls('w')))
    shading_elm_2 = parse_xml(r'<w:shd {} w:fill="a9a9a9"/>'.format(nsdecls('w')))
    shading_elm_3 = parse_xml(r'<w:shd {} w:fill="a9a9a9"/>'.format(nsdecls('w')))
    shading_elm_4 = parse_xml(r'<w:shd {} w:fill="a9a9a9"/>'.format(nsdecls('w')))
    shading_elm_5 = parse_xml(r'<w:shd {} w:fill="a9a9a9"/>'.format(nsdecls('w')))
    cells[0]._tc.get_or_add_tcPr().append(shading_elm_1)
    cells[1]._tc.get_or_add_tcPr().append(shading_elm_2)
    cells[2]._tc.get_or_add_tcPr().append(shading_elm_3)
    cells[3]._tc.get_or_add_tcPr().append(shading_elm_4)
    cells[4]._tc.get_or_add_tcPr().append(shading_elm_5)
    cells[0].text = 'Item'
    cells[1].text = 'Age'
    cells[2].text = 'Initial value'
    cells[3].text = 'Current value'
    cells[4].text = 'Rate'



def createdocx(res):
    fmtdata = formatData(res)
    document = Document()
    document.add_heading('Calculating Growth Rates', 2)
    growthtbl = document.add_table(rows=6, cols=5)
    creategrowthtable(growthtbl, res)
    tbl = document.add_table(rows=1, cols=1)
    row_cells = tbl.add_row().cells
    paragraph = row_cells[0].paragraphs[0]
    run = paragraph.add_run()
    run.add_picture(os.path.join(STATICFILES_DIRS[0], 'report/plots/eps.png'), width=Inches(1.80))
    run.add_picture(os.path.join(STATICFILES_DIRS[0], 'report/plots/ops.png'), width=Inches(1.80))
    run.add_picture(os.path.join(STATICFILES_DIRS[0], 'report/plots/sales.png'), width=Inches(1.80))
    table = document.add_table(rows=1, cols=2)
    pic_cells = table.rows[0].cells
    paragraph = pic_cells[0].paragraphs[0]
    run = paragraph.add_run()
    run.add_picture(os.path.join(STATICFILES_DIRS[0], 'report/plots/bvps.png'), width=Inches(1.80))
    tx_cells = table.rows[0].cells
    tx_cells[0].width = -1
    tx_cells[1].text = 'Companies that have been able to sustain compounded annual growth rates of over 10% per year ' \
                       'in EPS, OPS, Sales, and BVPS are considered to have a durable moat.  The competitive Moat ' \
                       'keeps competition at bay and allows these companies to grow at above average rates. Looking at the Growth Rates you have calculated, does this company have a Moat? Are all these growth rates similar? Reliable companies are ones where EPS, OPS, Sales, and BVPS, all grow at a similar rate.'
    document.save(os.path.join(STATICFILES_DIRS[0], 'report/stockreport.docx'))
    createpng('eps.png', fmtdata['eps'], 'Earning per share', 'year', 'EPS', 'eps')
    createpng('ops.png', fmtdata['ops'], 'Operating cash per share', 'year', 'OPS', 'ops')
    createpng('sales.png', fmtdata['sale'], 'Net Sale', 'year', 'sale', 'sale($, in millions)')
    createpng('bvps.png', fmtdata['bvps'], 'Book value per share', 'year', 'BVPS', 'bvps')
    createpng('net_income.png', fmtdata['net_income'], 'Net Income', 'year', 'Net Income', 'net income($, in millions)')
    createpng('ltdb.png', fmtdata['ltdebt'], 'Long-term Debt', 'year', 'long-term debt',
              'long-term debt($, in millions)')
    createpng('equity.png', fmtdata['equity'], 'Equity', 'year', 'Equity', 'equity($, in millions)')
    createpng('roic.png', fmtdata['roic'], 'ROIC', 'year', 'ROIC', 'roic, in %')
    createpng('roe.png', fmtdata['roe'], 'ROE', 'year', 'ROE', 'roe, in %')


def createpng(filename,data, title, xlabel, ylabel, legend):
    path = os.path.join(STATICFILES_DIRS[0], 'report/plots/' + filename)
    plot = figure(title=title, x_axis_label=xlabel, y_axis_label=ylabel, plot_width=400, plot_height=400)
    plot.line(data['x'], data['y'], legend=legend, color='red', line_width=2)
    export_png(plot, path)


def formatData(res):
    fmtdata = {}
    fmtdata.keys()
    dictkeys = list(res.keys())
    paramslist = ['eps', 'ops', 'sale', 'bvps', 'net_income', 'ltdebt', 'equity']

    for param in paramslist:
        xlist = []
        ylist = []
        for k in dictkeys:
            ylist.append(res[k][param])
            xlist.append(k)
        fmtdata[param] = {'x': xlist, 'y': ylist}

    # roe
    xlist = []
    ylist = []
    for k in dictkeys:
        ylist.append((res[k]['net_income'] * 100)/res[k]['equity'])
        xlist.append(k)
    fmtdata['roe'] = {'x': xlist, 'y': ylist}

    # roic
    xlist = []
    ylist = []
    for k in dictkeys:
        nopat = res[k]['operatingincome'] * (1-(res[k]['pift']/res[k]['ibpfit']))
        if k != 2008:
            investedcapital = ((res[k]['ltdebt'] + res[k]['commercialpaper'] + res[k]['currdebt'] + res[k]['equity'] -
                                (res[k]['cashequi']+res[k]['short_term'])) +
                               (res[k-1]['ltdebt'] + res[k-1]['commercialpaper'] + res[k-1]['currdebt'] + res[k-1][
                                   'equity'] -
                                (res[k-1]['cashequi']+res[k-1]['short_term']))) / 2
        else:
            investedcapital = (res[k]['ltdebt'] + res[k]['commercialpaper']+ res[k]['currdebt'] + res[k]['equity'] -
                                (res[k]['cashequi']+res[k]['short_term']))
        ylist.append(nopat*100/investedcapital)
        xlist.append(k)
    fmtdata['roic'] = {'x': xlist, 'y': ylist}
    return fmtdata
