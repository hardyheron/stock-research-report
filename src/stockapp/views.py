from django.shortcuts import render
from rest_framework.decorators import api_view
from django.http import JsonResponse
from rest_framework.status import HTTP_200_OK
from bokeh.plotting import figure
from bokeh.io import export_png

from .reportgenerator import get_request_data
from .CreateDocx import createdocx

# Create your views here.


def stockwebui(request):
    """
    this view function provide the index.html page for the basic
    form with generate pdf button.
    :param request:
    :return: render index.html template
    """
    return render(request, 'stockapp/index.html')


@api_view(['POST'])
def createreport(request):
    """
    This is the main function that is responsible of creating report of the stock.
    :param request:
    :return: Json response as 200, name of the report, url if report is generated correctly,
    other 450, not generated report
    """
    if request.method == 'POST':
        data = request.data
        res = get_request_data(data)
        createdocx(res)
        report_name = 'stockreport.docx'
        url = '/static/report/'+report_name
        x = [1, 3, 5, 7, 9, 11, 13]
        y = [1, 2, 3, 4, 5, 6, 7]
        title = 'y = f(x)'

        plot = figure(title=title,
                      x_axis_label='X-Axis',
                      y_axis_label='Y-Axis',
                      plot_width=400,
                      plot_height=400)

        plot.line(x, y, legend='f(x)', line_width=2)
        # Store components
        export_png(plot, filename="plot.png")
        return JsonResponse({"status": HTTP_200_OK, "reportname": report_name, 'url': url})
