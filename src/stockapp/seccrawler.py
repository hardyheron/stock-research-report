# This script will download all the 10-K, 10-Q and 8-K

from bs4 import BeautifulSoup
import requests


class SecCrawler:

    def __init__(self):
        self.hello = "Welcome to Sec Cralwer!"

    def filing_10K(self, companyCode, cik, priorto, count):
        # generate the url to crawl
        base_url = "http://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK="+str(cik)+"&type=10-K&dateb=" + \
                   str(priorto)+"&owner=exclude&output=xml&count="+str(count)
        print("started 10-K: "+str(companyCode))
        r = requests.get(base_url,  verify=False)
        yearurl = {}
        data = r.text
        base_url2 = "http://www.sec.gov/cgi-bin/browse-edgar?action=getcompany&CIK="+str(
            cik)+"&type=10-K&dateb="+str(priorto)+"&owner=exclude&start=10&output=xml&count="+str(count)
        r = requests.get(base_url2,  verify=False)
        data = data + r.text
        soup = BeautifulSoup(data, features="html.parser")  # Initializing to crawl again
        count = 10
        # If the link is .htm convert it to .html
        for link in soup.find_all('filing'):
            if '10-K' == link.find('type').text:
                datefiled = link.find('datefiled').text
                year = datefiled.split('-')[0]
                url = link.find('filinghref').text
                tmp_break = url.split('/')
                finalurl = '/'.join(tmp_break[:-1])
                yearurl[int(year)] = finalurl
                count -= 1
            if count < 1:
                break
        return yearurl
