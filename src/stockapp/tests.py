import os
import pandas_datareader.data as web
import datetime


def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print('Error: Creating directory. ' + directory)

    # read ticker symbols from a file to python symbol list


symbols = []
with open('C:\\Users\\samir\\Desktop\\volkovskaia\\STOCK_SYMBOL_LIST.txt') as f:
    for line in f:
        symbols.append(line.strip().split(',')[0])
f.close

# datetime is a Python module

# datetime.datetime is a data type within the datetime module
# which allows access to Gregorian dates and today function

# datetime.date is another data type within the datetime module
# which permits arithmetic with Gregorian date components

# definition of end with datetime.datetime type must precede
# definition of start with datetime.date type

# the start expression collects data that are up to five years old

end = datetime.datetime.today()

start = datetime.date(end.year - 3, 1, 1)

# set path for csv file
path_out = 'C:/Users/Michael Russo/stockprices/'

# loop through 50 tickers in symbol list with i values of 0 through 49

# if no historical data returned on any pass, try to get the ticker data again

# for first ticker symbol write a fresh copy of csv file for historical data
# on remaining ticker symbols append historical data to the file written for
# the first ticker symbol and do not include a header row


# while i<len(symbols):
for symbol in symbols:
    try:
        df = web.DataReader(symbol, 'yahoo', start, end)
        df = df.drop(['Adj Close'], axis=1)
        dir = path_out + symbol + '/'
        createFolder(dir)
        df.to_csv(dir + symbol + '.csv')
        print(symbol, 'has data stored to csv file')
    except:
        print("No information for ticker # and symbol:")
        print(symbol)
